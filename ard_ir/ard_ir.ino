#include <IRremote.h>

int		RECV_PIN = 3;
#define		IR_LED		2	// 10

IRrecv irrecv(RECV_PIN);

decode_results results;


void setup()
{

	/* add setup code here */
	Serial.begin(9600);
	irrecv.enableIRIn(); // Start the receiver
	pinMode(IR_LED, OUTPUT);	// configure led pin
	digitalWrite(IR_LED, LOW);

}

void loop()
{

	/* add main program code here */
	if (irrecv.decode(&results)) {
		digitalWrite(IR_LED, HIGH);
		Serial.println(results.value, HEX);
		irrecv.resume(); // Receive the next value
	}
	delay(100);
	digitalWrite(IR_LED, LOW);

}
